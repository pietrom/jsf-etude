package org.amicofragile.jsfetude.web.conditional;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean(name = "test")
@RequestScoped
public class TestBean {
	private Integer value;
	
	public void setValue(Integer value) {
		this.value = value;
	}
	
	public Integer getValue() {
		return value;
	}
}
