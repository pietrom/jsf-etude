package org.amicofragile.jsfetude.bookshelf;

public class Book {
	private final String title;
	private final String author;
	
	public Book(String title, String author) {
		this.title = title;
		this.author = author;
	}

	@Override
	public String toString() {
		return title;
	}

	public boolean hasAuthor(String test) {
		return author.equals(test);
	}
}
