package org.amicofragile.jsfetude.bookshelf;

import static org.junit.Assert.*;

import org.junit.Test;

public class BookTest {
	@Test
	public void createBookAndRenderToString() throws Exception {
		Book treasure = new Book("Treasure Island", "Robert Louis Stevenson");
		assertEquals("Treasure Island", treasure.toString());
	}
	
	@Test
	public void createBookAndTestAuthor() {
		Book treasure = new Book("Treasure Island", "Robert Louis Stevenson");
		assertTrue(treasure.hasAuthor("Robert Louis Stevenson"));
		assertFalse(treasure.hasAuthor("Daniel Defoe"));
	}
}
